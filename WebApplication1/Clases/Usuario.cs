﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Clases
{
    public class Usuario
    {
        
        private string contrasenia;
        private string email;
        private string nombre;
        private string apellido;
        private string codigoActivacion;

        
        public string Contrasenia { get { return contrasenia; } set { contrasenia = value; } }
        public string Email { get { return email; } set { email = value; } }
        public string Nombre { get { return nombre; } set { nombre = value; } }
        public string Apellido { get { return apellido; } set { apellido = value; } }
        public string CodigoActivacion { get { return codigoActivacion; } set { codigoActivacion = value; } }
        
    }
}