﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using WebApplication1.Clases;




namespace WebApplication1.Clases
{
    public class UsuarioService
    {

          Ado _ado;


          public UsuarioService()
        {
            _ado = new Ado();
        }


          
          public int Usuario_LoginUsuario(Usuario user)
          {
              
              ArrayList usuario = new ArrayList();
              usuario.Add(user.Email);
              usuario.Add(user.Contrasenia);
              int retorno = -1;
              int iduser;
              bool ok = _ado.EjecutarStoredProcedureDataReader("p_LoginUsuarioValido", usuario); 
              if (ok) 
              {
                  DataTable datos = _ado.DevolverDatosRapido(); 
                  if (datos.Rows.Count != 0) 
                  {
                      DataRow dr = datos.Rows[0];
                      
                      retorno = (Convert.ToInt32(dr["Estado"]));
                      iduser = (Convert.ToInt32(dr["IdUsuario"]));
                      HttpContext.Current.Session["varialbeuser"] = iduser;
                      
                  }
              }
              else
              { 
                  retorno = -1;
              }
             
              return retorno;

          }

          public int Usuario_RegistrarUsuario(Usuario user)
          {

              ArrayList usuario = new ArrayList();
              usuario.Add(user.Email);



              int retorno = -1;
              
              bool ok = _ado.EjecutarStoredProcedureDataReader("p_RegistrarUsuarioTP", usuario);
              if (ok)
              {
                  DataTable datos = _ado.DevolverDatosRapido();
                  if (datos.Rows.Count != 0)
                  {
                      DataRow dr = datos.Rows[0];

                      retorno = (Convert.ToInt32(dr["Estado"]));
                     

                  }
              }
              else
              {
                  retorno = -1;
              }

              return retorno;


             
              
          }


          public bool Usuario_RegistrarUsuarioCreate(Usuario usuario)
          {

              ArrayList datoscreate = new ArrayList();
              datoscreate.Add(usuario.Nombre);
              datoscreate.Add(usuario.Apellido);
              datoscreate.Add(usuario.Email);
              datoscreate.Add(usuario.Contrasenia);
              datoscreate.Add(usuario.CodigoActivacion);


              return _ado.EjecutarStoredProcedure(false, "p_CrearUsuarioTP", datoscreate);

          }

          public bool Usuario_RegistrarUsuarioAlter(Usuario usuario)
          {

              ArrayList datosalter = new ArrayList();
              datosalter.Add(usuario.Nombre);
              datosalter.Add(usuario.Apellido);
              datosalter.Add(usuario.Email);
              datosalter.Add(usuario.Contrasenia);
              datosalter.Add(usuario.CodigoActivacion);


              return _ado.EjecutarStoredProcedure(false, "p_CrearUsuarioAlter", datosalter);

          }


          public bool Usuario_RegistrarUsuarioActivar(Usuario usuarioact)
          {

              ArrayList datosact = new ArrayList();


     
              datosact.Add(usuarioact.CodigoActivacion);


              return _ado.EjecutarStoredProcedure(false, "p_ActivarUsuarioAlter", datosact);

          }

    }
}