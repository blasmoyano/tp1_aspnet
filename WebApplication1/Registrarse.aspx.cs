﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using WebApplication1.Clases;

namespace WebApplication1
{
    public partial class Registrarse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillCapctha();
            }
        }

        void FillCapctha()
        {
            try
            {
                Random random = new Random();
                string combination = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
                StringBuilder captcha = new StringBuilder();
                for (int i = 0; i < 6; i++)
                    captcha.Append(combination[random.Next(combination.Length)]);
                Session["captcha"] = captcha.ToString();
                imgCaptcha.ImageUrl = "GenerateCaptcha.aspx?" + DateTime.Now.Ticks.ToString();
            }
            catch
            {

                throw;
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            FillCapctha();
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            if (Session["captcha"].ToString() != txtCaptcha.Text)
            {
                Response.Write("Invalid Captcha Code");
            }
            else
            {
                Response.Write("Valid Captcha Code");
                FillCapctha();



               

            }
            Usuario usuario = new Usuario();
            UsuarioService userserv = new UsuarioService();
            usuario.Email = RegMail.Text;


            int estado = userserv.Usuario_RegistrarUsuario(usuario);

            if (estado == 1)
            {
                msj.Text = "Usuario ya creado";

            }
            else
            {
                if (estado == 0)
                {
                    

                    string cod = Guid.NewGuid().ToString();
                    string url = "http://" + Request.Url.Authority + "/ActivarUser.aspx?cod=" + cod;

                    HyperLink1.NavigateUrl = url;
                    HyperLink1.Text = "URL Activacion";

                    if (!string.IsNullOrEmpty(RegMail.Text))
                    {
                        Session["usuario"] = RegMail.Text;
                        Session["codigo"] = cod;
                    }

                    Usuario useralter = new Usuario();
                    UsuarioService userservalter = new UsuarioService();
                    useralter.Nombre = RegNombre.Text;
                    useralter.Apellido = RegApellido.Text;
                    useralter.Email = RegMail.Text;
                    useralter.Contrasenia = RegPass.Text;
                    useralter.CodigoActivacion = cod;
                    useralter.Email = RegMail.Text;

                    userservalter.Usuario_RegistrarUsuarioAlter(useralter);
                    msj.Text = "Su usuario ya existia, pero no estaba activado, se modificaron los datos. Por favor activarlo";


                }
                else
                {
                

                    string cod = Guid.NewGuid().ToString();
                    string url = "http://" + Request.Url.Authority + "/ActivarUser.aspx?cod=" + cod;

                    HyperLink1.NavigateUrl = url;
                    HyperLink1.Text = "URL Activacion";

                    if (!string.IsNullOrEmpty(RegMail.Text))
                    {
                        Session["usuario"] = RegMail.Text;
                        Session["codigo"] = cod;
                    }

                    Usuario usercreate = new Usuario();
                    UsuarioService userservcreate= new UsuarioService();

                    usercreate.Nombre = RegNombre.Text;
                    usercreate.Apellido = RegApellido.Text;
                    usercreate.Email = RegMail.Text;
                    usercreate.Contrasenia = RegPass.Text;
                    usercreate.CodigoActivacion = cod;
                    userservcreate.Usuario_RegistrarUsuarioCreate(usercreate);
                    

                    msj.Text = "Usuario creado con exito, por favor recuerde activarlo.";
                }
            }

          
            
        }

    }
}