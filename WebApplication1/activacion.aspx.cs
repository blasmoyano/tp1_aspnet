﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class activacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var cod = Request.QueryString["cod"] ?? ""; // ?? operador verifica  si lo de la izq es null ejecuta lo de la derecha
            if (!string.IsNullOrEmpty(cod))
            {
                Label1.Text = "Usuario Activado";

            }
            else { Label1.Text = "Codigo Incorrecto"; }
        }
    }
}