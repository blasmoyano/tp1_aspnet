﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Clases;

namespace WebApplication1
{
    public partial class EliminarAlbum : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsi_Click(object sender, EventArgs e)
        {
            AlbumService albserv = new AlbumService();
            int id = Convert.ToInt32(Session["eliminaralbum"]);


            bool ok = albserv.Album_Eliminar(id);

            if (ok == true)
            {

                Response.Redirect("ListarAlbum.aspx");
               }
          

        }

        protected void btnno_Click(object sender, EventArgs e)
        {
            Response.Redirect("ListarAlbum.aspx");
        }
    }
}