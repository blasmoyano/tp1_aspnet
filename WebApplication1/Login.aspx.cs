﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Clases;

namespace WebApplication1
{
    public partial class Login : System.Web.UI.Page
    {
        UsuarioService userserv = new UsuarioService();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void tp_login_entrar_Click(object sender, EventArgs e)
        {
            Usuario usuario = new Usuario();
            usuario.Email = userName.Text;
            usuario.Contrasenia = passWord.Text;

            int estado = userserv.Usuario_LoginUsuario(usuario);

            if (estado == 1)
            {
                Session["Usuario"] = usuario.Email;
                Response.Redirect("ListarCancion.aspx");
                
            }
            else
            {
                if (estado == 0)
                {
                    msj.Text = "Usuario inactivo";
                }
                else
                {
                    msj.Text = "Verifique usuario y/o contraseña";
                }
            }
        }
    }
}