﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class ConMenu : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            identificacionUser.Text = Convert.ToString(Session["Usuario"]);
           
        }

         protected void logout_Click(object sender, EventArgs e)
        {
            
            Session.Clear();
            Response.Redirect("login.aspx");
        }
    }
}