USE [master]
GO
CREATE DATABASE [TP20142C]
GO
ALTER DATABASE [TP20142C] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TP20142C].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TP20142C] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [TP20142C] SET ANSI_NULLS OFF
GO
ALTER DATABASE [TP20142C] SET ANSI_PADDING OFF
GO
ALTER DATABASE [TP20142C] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [TP20142C] SET ARITHABORT OFF
GO
ALTER DATABASE [TP20142C] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [TP20142C] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [TP20142C] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [TP20142C] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [TP20142C] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [TP20142C] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [TP20142C] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [TP20142C] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [TP20142C] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [TP20142C] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [TP20142C] SET  DISABLE_BROKER
GO
ALTER DATABASE [TP20142C] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [TP20142C] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [TP20142C] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [TP20142C] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [TP20142C] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [TP20142C] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [TP20142C] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [TP20142C] SET  READ_WRITE
GO
ALTER DATABASE [TP20142C] SET RECOVERY SIMPLE
GO
ALTER DATABASE [TP20142C] SET  MULTI_USER
GO
ALTER DATABASE [TP20142C] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [TP20142C] SET DB_CHAINING OFF
GO
USE [TP20142C]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 07/13/2014 06:23:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[IdUsuario] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](20) NOT NULL,
	[Apellido] [nvarchar](20) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Contrasenia] [nvarchar](50) NOT NULL,
	[Estado] [smallint] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaActivacion] [datetime] NOT NULL,
	[CodigoActivacion] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [TP20142C]
GO

/****** Object:  Table [dbo].[Album]    Script Date: 8/31/2014 3:39:26 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
USE [TP20142C]


IF OBJECT_ID('Cancion', 'U') IS NOT NULL
	DROP TABLE Cancion;

IF OBJECT_ID('Album', 'U') IS NOT NULL
	DROP TABLE [Album];

/****** Object:  Table [dbo].[Album]    Script Date: 24/09/2014 7:09:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Album](
	[IdAlbum] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](20) NOT NULL,
	[Anio] [int] NOT NULL,
	[FechaCreación] [datetime] NOT NULL,
	[IdUsuario] [int] NOT NULL,
 CONSTRAINT [PK_Album] PRIMARY KEY CLUSTERED 
(
	[IdAlbum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Album]  WITH CHECK ADD  CONSTRAINT [FK_Album_Usuarios] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO

ALTER TABLE [dbo].[Album] CHECK CONSTRAINT [FK_Album_Usuarios]
GO


/****** Object:  Table [dbo].[Cancion]    Script Date: 8/31/2014 3:40:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Cancion](
	[IdCancion] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](20) NOT NULL,
	[Duracion] [int] NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[IdAlbum] [int] NULL,
 CONSTRAINT [PK_Cancion] PRIMARY KEY CLUSTERED 
(
	[IdCancion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Cancion]  WITH CHECK ADD  CONSTRAINT [FK_Cancion_Album] FOREIGN KEY([IdAlbum])
REFERENCES [dbo].[Album] ([IdAlbum])
GO

ALTER TABLE [dbo].[Cancion] CHECK CONSTRAINT [FK_Cancion_Album]
GO


/****** Object:  StoredProcedure [dbo].[p_RegistrarUsuario]    Script Date: 07/13/2014 06:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[p_RegistrarUsuario]
@nombre nvarchar(20),
@apellido nvarchar(20),
@email nvarchar(50),
@contrasenia nvarchar(50),
@estado smallint,
@fechaCreacion datetime,
@fechaActivacion datetime,
@codigoActivacion nvarchar(50)

as
begin

SET NOCOUNT ON;
	IF EXISTS(SELECT idUsuario FROM Usuarios WHERE Email = @email and Estado=1)
	BEGIN
		SELECT -1 -- Username exists.
	END
	ELSE IF EXISTS(SELECT idUsuario FROM Usuarios WHERE Email = @email and Estado=0)
	BEGIN
		update Usuarios set Nombre=@nombre, Apellido=@apellido, Contrasenia=@contrasenia
		where Usuarios.IdUsuario = IdUsuario
	END
	ELSE
	BEGIN
		insert into Usuarios(Nombre, Apellido, Email, Contrasenia, Estado, FechaCreacion, FechaActivacion, CodigoActivacion)
		values (@nombre, @apellido, @email, @contrasenia,@estado, @fechaCreacion, @fechaActivacion, @codigoActivacion)
		select SCOPE_IDENTITY() as idUsuario			   
     END
end
GO
/****** Object:  StoredProcedure [dbo].[p_ObtenerUsuario]    Script Date: 07/13/2014 06:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[p_ObtenerUsuario]
@idUsuario int 
as
begin
select * from Usuarios where IdUsuario = @idUsuario
end
GO
/****** Object:  StoredProcedure [dbo].[p_loginUsuario]    Script Date: 07/13/2014 06:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_loginUsuario]
      @email NVARCHAR(50),
      @contrasenia NVARCHAR(50)
AS
BEGIN
      SET NOCOUNT ON;
      DECLARE @estado INT, @idUsuario int
     
      SELECT @estado = Estado, @idUsuario = IdUsuario
      FROM Usuarios WHERE Email = @email AND Contrasenia = @contrasenia
     
      IF @idUsuario IS NOT NULL
      BEGIN
            IF @estado = 1
            BEGIN
                  SELECT @idUsuario [idUsuario] -- User Valid
            END
            ELSE
            BEGIN
                  SELECT -2 -- User not activated.
            END
      END
      ELSE
      BEGIN
            SELECT -1 -- User invalid.
      END
END
GO
/****** Object:  StoredProcedure [dbo].[p_Activacion]    Script Date: 07/13/2014 06:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[p_Activacion]
@codigoActivacion nvarchar(50)
as
begin
declare @idUsuario int
select @idUsuario = idUsuario from Usuarios where CodigoActivacion = @codigoActivacion
begin
update Usuarios set Estado = 1 where IdUsuario = @idUsuario
end
end
GO

