--si tienen registros en la tabla Cancion, 
--deben de tener al menos un usuario en la tabla usuarios para que funcione

DECLARE @idUsuario INTEGER

IF(EXISTS(SELECT * FROM Cancion))
BEGIN
	IF(EXISTS(SELECT * FROM Usuarios))
	BEGIN
		SELECT TOP 1 @idUsuario = IdUsuario FROM Usuarios
	END

	Alter table Cancion
	Add IdUsuario INTEGER NULL;
	
	 EXEC ('UPDATE Cancion SET IdUsuario = ' + @idUsuario)	

	Alter table Cancion
	ALTER COLUMN IdUsuario INTEGER NOT NULL
END
ELSE
BEGIN
	Alter table Cancion
	ADD IdUsuario INTEGER NOT NULL
END

ALTER TABLE dbo.Cancion ADD CONSTRAINT
	FK_Cancion_Usuarios FOREIGN KEY
	(
	IdUsuario
	) REFERENCES dbo.Usuarios
	(
	IdUsuario
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION


